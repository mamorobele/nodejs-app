# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Decentralized applications using React.js
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Set Up

Add ssh-keys
- How to generate keys

 on linux
 1. ssh-keygen
 
 Generating public/private rsa key pair.
 Enter file in which to save the key (/Users/emmap1/.ssh/id_rsa): /Users/emmap1/.ssh/my-new-ssh-key
 
 2. Copy content of id_rsa id_rsa.pub
 
 3. Paste on settings/General/Access Keys/Add keys
 
 Then clone project
 git clone git@bitbucket.org:mamorobele/blockchain-dapps.git


* Configurations

  creating react app
  
  - sudo npm i npx -g
  
  - npx create-react-app
  
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jeremiah 0815401252
* Other community or team contact

### Notes About React
React is a javascript library. And is component based, uses components as building blocks.
Uses render() method to accepts input and controls what to display.
React can also help with managing state of your application.